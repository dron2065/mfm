#ifndef MFM_GENERAL_H
#define MFM_GENERAL_H

#include <sys/types.h>
#include <sys/stat.h>
#include "mfm_tab.h"

/**
 * Global state of session
 */
typedef struct
{
    mfm_tab* tabs;         /**< Array of current menus */
    int len;               /**< Count of menus */
    int cur;               /**< Current active menu */
    int file_commands_len; /**< Count of commands for different values */
    char*** file_commands; /**< Commands for files */
    char** bookmarks;      /**< Bookmarks for fast jump */
    int bookmarks_len;     /**< Count of bookmarks */
} mfm_state;

/**
 * Substitute %f by selected files and %% by %
 * @param menu Current tab with items
 * @param orig Original string
 * @return
 */
char* mfm_substitute(mfm_tab* menu, char* orig);

/**
 * Read config file with actions for files of
 * different types
 * @param f_cmd Place to put result
 */
char*** mfm_read_commands(int* result_len);

/**
 * Get the screen size
 * @param h Place to put height
 * @param w Place to put width
 */
void mfm_scr_size(int* h, int* w);

/**
 * Execute command in shell
 * @param comm
 */
void mfm_command(char* comm);

/**
 * Show message in the bot of window
 * @param message to show
 * @param delay after showing
 */
void mfm_show_message(char* message, int delay);

/**
 * Append two paths with / . You shoul free result of this function
 * after using
 * @param one
 * @param two
 * @return
 */
char* mfm_append_paths(char* one, char* two);

#define MFM_TRAVERS_WITH_STAT 0
#define MFM_TRAVERS_WITH_LSTAT 1

/**
 * Traverse the directory
 * @param path          Target directory
 * @param stat_function What function should use for extracting file info (stat or lstat)
 * @param fn            Function to be applied to every item
 * @param udata         User data for function
 * @return
 */
int mfm_traverse_directory(
    char* path,
	int stat_function,
    int (*fn)(char*, struct stat*, void*),
    void* udata
);

/**
 * Is it '.' or '..' directory
 * @param path
 * @return
 */
int mfm_dots_dir(char* path);

/**
 * Draw help line
 * @param h Coords where draw it
 * @param w
 */
void mfm_help(int h, int w);

/**
 * Draw the tab numbers
 * @param st
 * @param h Coords where to draw
 * @param w
 */
void mfm_draw_numbers(mfm_state* st, int h, int w);

/**
 * Read the bookmarks
 * @return
 */
char** mfm_read_bookmarks(int* len);

/**
 * Create the empty directory
 * @param name
 * @return
 */
int mfm_mkdir(char* name);

/**
 * Delete the item (callback for traverse function)
 * @param path
 * @param st
 * @param udata
 * @return 0 - deleting successful, 1 - something going wrong
 */
int mfm_delete_item(char* path);

/**
 * Travers all selected items, except the current tab
 * @param st
 * @param cb
 * @param udata
 */
void mfm_traverse_all_selected(
    mfm_state* st,
    void (*cb)(mfm_tab*, mfm_tab_item*, void*),
    void* udata
);

/**
 * Get the text presentation of size in bytes
 * @param bytes
 * @param buf
 */
void mfm_get_size_text(unsigned long long bytes, char* buf);

/**
 * Drain all data at standard input
 */
void mfm_drain_input();

/**
 * Get command for file
 * @param file_name
 * @param file_commands_len
 * @param file_commands
 * @return
 */
char* mfm_get_command_for_file(
    char* file_name,
    int file_commands_len,
    char*** file_commands
);

/**
 * Set cursor of tab on file
 */
void mfm_pos_tab_on_item(mfm_tab* tab, char* item);

/**
 * Get directory item 
 */
char* mfm_get_directory_item(char* path, int index);

#endif /* MFM_GENERAL_H */

