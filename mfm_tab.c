#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <search.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <grp.h>
#include <pwd.h>

#include "mfm_general.h"
#include "mfm_tab.h"

/**
 * Is file executable?
 * @param fname Target file
 * @param st    stat for file
 * @param file_commands_len
 * @param file_commands
 * @return
 */
mfm_tab_item_props mfm_is_exec(
    char* file_name,
    struct stat* st,
    int file_commands_len,
    char*** file_commands
) {
    // If command for file exists - no executable
    if (mfm_get_command_for_file(file_name, file_commands_len, file_commands)) {
        return MFM_NON;
    }

    // Check is file executable for current user
    if (!access(file_name, X_OK)) {
        return MFM_EXE;
    }

    //Finally
    return MFM_NON;
}

/**
 * Comparation function for sorting entries in directory
 * @param one
 * @param two
 * @return
 */
int mfm_cmp_tab_items(const void* one, const void* two)
{
    mfm_tab_item_props p_one, p_two;
    p_one = ((mfm_tab_item*)one)->props;
    p_one &= MFM_DIR;
    p_two = ((mfm_tab_item*)two)->props;
    p_two &= MFM_DIR;
    if (p_one == MFM_DIR && p_two != MFM_DIR) {
        return -1;
    }
    if (p_one != MFM_DIR && p_two == MFM_DIR) {
        return 1;
    }
    return strcmp(
        ((mfm_tab_item*)one)->text,
        ((mfm_tab_item*)two)->text
    );
}

/**
 * Destroy the tab
 * @param tab
 */
void mfm_destroy_tab(mfm_tab* tab)
{
    if (!tab) {
        return;
    }
    free(tab->dir);
    int i = 0;
    mfm_tab_item* it;
    if (tab->items) {
        while (i < tab->len) {
            free(tab->items[i].text);
            i++;
        }
    }
    free(tab->items);
}

typedef struct
{
    int current_count;
    int is_hidden;
} mfm_count_directory_items_udata;

int mfm_count_directory_items(char* name, struct stat* st, void* udata);

/**
 * User data for directory traverse callback
 */
typedef struct
{
    mfm_tab* tab;
    int file_commands_len;
    char*** file_commands;
} mfm_init_tab_item_udata;

int mfm_init_tab_item(char* name, struct stat* st, void* udata);

//Create the tab
int mfm_init_tab(mfm_tab* tab, char*** file_commands, int file_commands_len)
{
    //Clear this menu
    mfm_destroy_tab(tab);

    //Set current directory of menu
    tab->dir = malloc(256);
    tab->dir = getcwd(tab->dir, 256);
    tab->dir = realloc(tab->dir, strlen(tab->dir) + 1);

    //Set active element and position of view
    tab->act = 0;
    tab->pos_view = 0;

    //Count items at directory
    mfm_count_directory_items_udata udata_count;
    udata_count.is_hidden = tab->hidden;
    udata_count.current_count = 1;
    mfm_traverse_directory(
        ".",
        MFM_TRAVERS_WITH_STAT,
        mfm_count_directory_items,
        (void*)&udata_count
    );

    // Take place for items
    tab->items = calloc(udata_count.current_count, sizeof(mfm_tab_item));

    //Struct for info about file
    tab->items[0].props = MFM_DIR;
    tab->items[0].text = malloc(3);
    tab->items[0].text[0] = '.';
    tab->items[0].text[1] = '.';
    tab->items[0].text[2] = '\0';
    tab->len = 1;

    //Traverse directory
    mfm_init_tab_item_udata udata_fill;
    udata_fill.tab = tab;
    udata_fill.file_commands = file_commands;
    udata_fill.file_commands_len = file_commands_len;
    mfm_traverse_directory(
        ".",
        MFM_TRAVERS_WITH_STAT,
        mfm_init_tab_item,
        (void*)&udata_fill
    );

    //Sort list (directories in the beginning)
    qsort(
        tab->items,
        tab->len,
        sizeof(mfm_tab_item),
        mfm_cmp_tab_items
    );

    //Finish work
    return 0;
}

/**
 * Count items at directory
 */
int mfm_count_directory_items(char* name, struct stat* st, void* udata)
{
    name += 2;
    int is_hidden = ((mfm_count_directory_items_udata*)udata)->is_hidden;
    if (mfm_dots_dir(name)) {
        return 0;
    }
    if ((!is_hidden) && (name[0] == '.')) {
        return 0;
    }
    ((mfm_count_directory_items_udata*)udata)->current_count++;
    return 0;
}

/**
 * Callback for directory traversing
 * @param name
 * @param st
 * @param udata
 * @return
 */
int mfm_init_tab_item(char* name, struct stat* st, void* udata)
{
    mfm_tab* tab = ((mfm_init_tab_item_udata*)udata)->tab;
    name += 2;
    char*** file_commands = ((mfm_init_tab_item_udata*)udata)->file_commands;
    int file_commands_len = ((mfm_init_tab_item_udata*)udata)->file_commands_len;

    if (mfm_dots_dir(name)) {
        return 0;
    }
    if ((!tab->hidden) && (name[0] == '.')) {
        return 0;
    }

    //Save the name of file
    tab->items[tab->len].text = malloc(strlen(name) + 1);
    strcpy(tab->items[tab->len].text, name);

    //Is it directory?
    if (S_ISDIR(st->st_mode)) {
        tab->items[tab->len].props = MFM_DIR;
    } else if (S_ISREG(st->st_mode)) {
        tab->items[tab->len].props = MFM_REG;
        //Or executable?
        tab->items[tab->len].props |= mfm_is_exec(name, st, file_commands_len, file_commands);
    }

    tab->len++;

    //Finish work
    return 0;
}

/**
 * Draw the current tab content
 * @param tab
 * @param h
 * @param w
 */
void mfm_draw_tab(
    mfm_tab* tab, //Menu to drawing
    int h,         //Screen size
    int w
) {
    //Draw the directory in the head
    printf("\e[1;1H\e[32;49m\e[2K%s", tab->dir);

        //Position on the screen
    int scr_pos,
        //Position in the menu
        tab_pos = tab->pos_view,
        //Offset by empty line between dirs and files
        off = 0;

    //Lines with text
    for(
        scr_pos = 2;
        scr_pos <= h - 2 && tab_pos < tab->len;
        tab_pos++, scr_pos++
    ) {
        if (off != !(tab->items[tab_pos].props & MFM_DIR)) {
            printf("\e[33;49m\e[%i;1H", scr_pos);
            for (int i = 1; i <= w; i++) {
                printf("%s", "─");
            }
        }
        off = !(tab->items[tab_pos].props & MFM_DIR);
        printf(
            "\e[%i;1H\e[%i;%im",
            scr_pos + off,
            tab->items[tab_pos].props & MFM_SEL ? 33 :
                tab->items[tab_pos].props & MFM_EXE ? 36 : 37,
            tab_pos == tab->act ? 42 : 49);
        printf("%s\e[0K", tab->items[tab_pos].text);
    }

    //Empty lines
    if (!off) {
        printf("\e[33;49m\e[%i;1H", scr_pos);
        for (int i = 1; i <= w; i++) {
            printf("%s", "─");
        }
    }
    printf("%s", "\e[49m");
    scr_pos++;
    for (; scr_pos <= h - 1; scr_pos++) {
        printf("\e[%i;1H\e[2K", scr_pos);
    }

    //Show current position in menu
    printf(
        "\e[33;49m\e[1;%iH\e[0K%i%c",
        w - 3,
        (tab->act + 1) * 100 / tab->len,
        '%'
    );
}

/**
 * Correct tab params
 * @param tab
 * @param h
 * @param w
 */
void mfm_correct_tab(mfm_tab* tab, int h, int w)
{
    if (tab->act < 0) {
        tab->act = 0;
    } else if (tab->act > tab->len - 1) {
        tab->act = tab->len - 1;
    }
    if (tab->pos_view > tab->act) {
        tab->pos_view = tab->act;
        return;
    }
    if (tab->act - tab->pos_view > h - 4) {
        tab->pos_view = tab->act - h + 4;
    }
}
