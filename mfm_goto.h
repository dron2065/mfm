#ifndef MFM_GOTO_H
#define MFM_GOTO_H

/**
 * Go to the some directory
 * @param bookmarks
 */
void mfm_goto(char** bookmarks, int bookmarks_len);

/**
 * Input directory and goto
 * @param h
 * @param w
 */
void mfm_input_goto();

#endif /* MFM_GOTO_H */
