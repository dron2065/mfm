#ifndef MFM_SIMPLE_MENU_H
#define MFM_SIMPLE_MENU_H

/**
 * Run simple menu
 * @param variants for selecting
 * @return select variant (-1 for denie)
 */
int mfm_simple_menu(char** variants, int variants_len);

#endif /* MFM_SIMPLE_MENU_H */
