#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mfm_goto.h"
#include "mfm_general.h"
#include "mfm_input.h"
#include "mfm_simple_menu.h"

/**
 * Goto to the something directory
 * @param bookmarks
 */
void mfm_goto(char** bookmarks, int bookmarks_len)
{
    //Draw the head
    printf("%s", "\e[1;1H\e[33;41m\e[2K");
    printf("%s", "Go to the directory:");

    //If there is no bookmarks - input directory
    //and go to it
    if (!bookmarks) {
        mfm_input_goto();
        return;
    }

    //Select the bookmark from menu
    int choice = mfm_simple_menu(bookmarks, bookmarks_len);
    if (choice == -1) {
        return;
    }
    int res = chdir(bookmarks[choice]);
}

/**
 * Input directory and goto
 * @param h
 * @param w
 */
void mfm_input_goto()
{
    char* dir = mfm_read_line(NULL);
    if (dir) {
        int res = chdir(dir);
        free(dir);
    }
}
