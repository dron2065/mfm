#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <search.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include "mfm_general.h"
#include "mfm_input.h"

char* mfm_all_selected(mfm_tab* tab);
int mfm_subs_count(char* orig, int files_len);
void mfm_subs_write(char* dest, char* orig, char* files);

/**
 * Substitute all %f by selected files and %%f by %f
 * @param tab
 * @param orig
 * @return
 */
char* mfm_substitute(mfm_tab* tab, char* orig)
{
    char* files = mfm_all_selected(tab);
    int files_len = strlen(files);

    char* res = malloc(mfm_subs_count(orig, files_len) + 1);
    mfm_subs_write(res, orig, files);

    free(files);

    return res;
}

int mfm_shell_len(char* text);
char* mfm_write_single_item(char* dest, char* item);
/**
 * Form the selected items to single string
 * @param tab
 * @return
 */
char* mfm_all_selected(mfm_tab* tab)
{
    int r_len = 0;

    //Count the total len
    for (int i = 0; i < tab->len; i++) {
        if (tab->items[i].props & MFM_SEL) {
            r_len += mfm_shell_len(tab->items[i].text);
        }
    }
    if (!r_len) {
        r_len += mfm_shell_len(tab->items[tab->act].text);
    }

    //Write the result
    char* res = malloc(r_len);
    char* cur = res;
    for (int i = 0; i < tab->len; i++) {
        if (tab->items[i].props & MFM_SEL) {
            cur = mfm_write_single_item(cur, tab->items[i].text);
        }
    }
    if (cur == res) {
        cur = mfm_write_single_item(cur, tab->items[tab->act].text);
    }

    *--cur = '\0';

    return res;
}

/**
 * Count len of string in the shell command
 * @param text
 * @return
 */
int mfm_shell_len(char* text)
{
    int res = 3;
    for (char* c = text; *c; c++) {
        switch (*c) {
        case '\\':
        case '"':
            res += 2;
            break;
        default:
            res++;
            break;
        }
    }
    return res;
}

/**
 * Write to the buffer single item
 * @param dest
 * @param item
 * @return Current position
 */
char* mfm_write_single_item(char* dest, char* item)
{
    *dest++ = '"';
    for (char* c = item; *c; c++) {
        switch (*c) {
        case '\\':
            *dest++ = '\\';
            *dest++ = '\\';
            break;
        case '"':
            *dest++ = '\\';
            *dest++ = '"';
            break;
        default:
            *dest++ = *c;
            break;
        }
    }
    *dest++ = '"';
    *dest++ = ' ';
    return dest;
}

/**
 * Count total len
 * @param orig
 * @param files_len
 * @return
 */
int mfm_subs_count(char* orig, int files_len)
{
    int res = strlen(orig);
    char* cur = orig;
    while ((cur = strstr(cur, "%f"))) {
        if (cur == orig) {
            res = res - 2 + files_len;
        } else if (cur[-1] == '%') {
            res--;
        } else {
            res = res - 2 + files_len;
        }
        cur += 2;
    }
    return res;
}

/**
 * Write data to the format string
 * @param dest
 * @param orig
 * @param files
 */
void mfm_subs_write(char* dest, char* orig, char* files)
{
    int files_len = strlen(files);
    char* prev = orig;
    char* next;
    while ((next = strstr(prev, "%f"))) {
        if (next == orig) {
            strcpy(dest, files);
            dest += files_len;
        } else if (next[-1] == '%') {
            int delta = next - 1 - prev;
            memcpy(dest, prev, delta);
            dest += delta;
            *dest++ = '%';
            *dest++ = 'f';
        } else {
            int delta = next - prev;
            memcpy(dest, prev, delta);
            dest += delta;
            strcpy(dest, files);
            dest += files_len;
        }
        prev = next + 2;
    }
    strcpy(dest, prev);
}

/**
 * Cut the \r\n symbols from string
 * @param str
 */
void mfm_cut_1310(char* str)
{
    int l = strlen(str);
    for (int i = l - 1; i >= l - 2; i--) {
        if (str[i] == '\n' || str[i] == '\r') {
            str[i] = '\0';
        }
    }
}

/**
 * Read config file with actions for files of different types
 * @param f_cmd
 */
char*** mfm_read_commands(int* result_len)
{
    //Find the file
    char* fname = mfm_append_paths(getenv("HOME"), ".local/etc/mfm/ext");
    FILE* f = fopen(fname, "r");
    free(fname);
    if (!f) {
        return NULL;
    }

    // Init the result
    char ***result = NULL;
    int result_cursor = 0;

    //Read line by line
    char buf[1024];
    char* last = NULL;
    for (;;) {
        //Read the line
        if (!fgets(buf, 1024, f)) {
            break;
        }

        //Skip if it's not full line
        int len = strlen(buf);
        if (buf[len - 1] != '\n' && !feof(f)) {
            continue;
        }

        // Find the delimiter in the string
        char* delim = strchr(buf, '=');

        if (delim != NULL) {
            //Skip if line starts with '='
            if (buf == delim) {
                continue;
            }

            // Skip if line ends with '='
            if ((delim - buf) == (strlen(buf) - 1)) {
                continue;
            }
        // Skip if command not specified
        } else if (!last) {
            continue;
        }

        mfm_cut_1310(buf);

        //Form key-value element
        char** kv = malloc(sizeof(char*) * 2);
        if (delim) {
            *(delim++) = '\0';
            kv[1] = malloc(strlen(delim) + 1);
            strcpy(kv[1], delim);
            last = kv[1];
        } else {
            kv[1] = malloc(strlen(last + 1));
            strcpy(kv[1], last);
        }
        kv[0] = malloc(strlen(buf) + 1);
        strcpy(kv[0], buf);

        // Save key value element
        result = realloc(result, (result_cursor + 1) * sizeof(char**));
        result[result_cursor++] = kv;
    }

    //Finish work
    fclose(f);
    *result_len = result_cursor;

    return result;
}

/**
 * Get the size of screen
 * @param h
 * @param w
 */
void mfm_scr_size(int* h, int* w)
{
    *h = 0;
    *w = 0;
    mfm_drain_input();
    printf("%s", "\e[1000;5000H\e[6n");
    int *t = h, res;
    for (char c = '\0'; c != 'R'; res = read(0, &c, 1)) {
        if (c == ';') {
            t = w;
        } else if (c >= '0' && c <= '9') {
            *t = *t * 10 + c - 48;
        }
    }
    mfm_drain_input();
}

/**
 * Execute command in shell
 * @param comm
 */
void mfm_command(char* comm)
{
    int res;
    printf("%s", "\ec");
    res = system("stty sane;stty echo");
    res = system(comm);
    printf("%s", "\ec");
    res = system("stty raw;stty -echo");
    printf("%s", "\e[1m\e[?25l");
    mfm_drain_input();
}

/**
 * Show message in the bot of window
 * @param message
 * @param delay
 */
void mfm_show_message(char* message, int delay)
{
    int h, w;
    mfm_scr_size(&h, &w);
    printf("\e[1;1H\e[49;32m\e[2J%s", message);
    char buf[8];
    mfm_key key;
    if (delay) {
        mfm_read_key(buf, &key);
    }
}

/**
 * Append two paths with /
 * @param one
 * @param two
 * @return
 */
char* mfm_append_paths(char* one, char* two)
{
    char* res = malloc(
        strlen(one) +
        strlen(two) +
        2
    );
    sprintf(res, "%s/%s", one, two);
    return res;
}

/**
 * List of strings
 */
typedef struct string_list_struct
{
    char* str;
    struct string_list_struct* next;
} string_list;

/**
 * Traverse the directory
 * @param path          Target directory
 * @param stat_function What function should use for extracting file info (stat or lstat)
 * @param fn            Function to be applied to every item
 * @param udata         User data for function
 * @return
 */
int mfm_traverse_directory(
    char* path,
	int stat_function,
    int (*fn)(char*, struct stat*, void*),
    void* udata
) {
    char* prev_item = NULL;
    char* next_item = NULL;
    int i = 0;
    for (;;) {
        next_item = mfm_get_directory_item(path, i);
        if (!next_item) {
            break;
        }
        if (prev_item) {
            if (!strcmp(prev_item, next_item)) {
                i++;
                free(next_item);
                continue;
            }
        }
        struct stat st;
        if ((stat_function ? lstat : stat)(next_item, &st) != -1) {
            if (fn(next_item, &st, udata) < 0) {
                free(next_item);
                break;
            }
        }
        free(prev_item);
        prev_item = next_item;
    }
    free(prev_item);
    return 0;
}

/**
 * Is it '.' or '..' directory
 * @param path
 * @return
 */
int mfm_dots_dir(char* path)
{
    char* last = strrchr(path, '/');
    if (last) {
        path = last + 1;
    }
    if (!strcmp(path, ".") || !strcmp(path, "..")) {
        return 1;
    }
    return 0;
}

/**
 * Draw help line
 * @param h
 * @param w
 */
void mfm_help(int h, int w)
{
    printf("\e[%i;12H\e[49m\e[2K", h);
    printf("%s", "\e[32mMK\e[36mD\e[32mIR ");
    printf("%s", "\e[36mT\e[32mOUCH ");
    printf("%s", "\e[36mE\e[32mDIT ");
    printf("%s", "\e[36mL\e[32mESS ");
    printf("%s", "\e[36mS\e[32mHELL ");
    printf("%s", "\e[36mR\e[32mENAME ");
    printf("%s", "\e[36mU\e[32mPDATE ");
    printf("%s", "\e[36mH\e[32mIDDEN ");
    printf("%s", "\e[36mG\e[32mOTO ");
    printf("%s", "\e[36mJ\e[32mUMP ");
    printf("%s", "\e[36mC\e[32mOPY ");
    printf("%s", "\e[36mM\e[32mOVE ");
    printf("%s", "\e[36mQ\e[32mUIT ");
}

/**
 * Draw the tab numbers
 * @param st
 * @param h
 * @param w
 */
void mfm_draw_numbers(mfm_state* st, int h, int w)
{
    int i = 0;
    printf("\e[%i;1H", h);
    while (i < st->len) {
        if (i == st->cur) {
            printf("%s", "\e[31;46m ");
        } else {
            printf("\e[33;49m%i", i);
        }
        i++;
    }
}

/**
 * Read the bookmarks
 * @return
 */
char** mfm_read_bookmarks(int* len)
{
    char* fname = mfm_append_paths(getenv("HOME"), ".local/etc/mfm/bookmarks");
    FILE* f = fopen(fname, "r");
    free(fname);
    if (!f) {
        return NULL;
    }
    int cur = 0;
    char** res = NULL;
    char buf[256];
    while (!feof(f)) {
        if (!fgets(buf, 256, f)) {
            continue;
        }
        mfm_cut_1310(buf);
        int l = strlen(buf);
        if (!l) {
            continue;
        }
        res = realloc(res, (cur + 1) * sizeof(char*));
        res[cur] = malloc(l + 1);
        strcpy(res[cur], buf);
        cur++;
    }

    fclose(f);

    *len = cur;
    return res;
}

/**
 * Delete the item
 * @param path
 * @param st
 * @param udata
 * @return
 */
int mfm_delete_item(char* path)
{
    if (mfm_dots_dir(path)) {
        return 0;
    }
    struct stat st;
    if (lstat(path, &st) == -1) {
        return 0;
    }
    if (S_ISDIR(st.st_mode)) {
        int i = 0;
        char* next_item;
        while ((next_item = mfm_get_directory_item(path, i))) {
            i += mfm_delete_item(next_item);
            free(next_item);
        }
        if (rmdir(path) == -1) {
            mfm_show_message(path, 1);
            mfm_show_message(strerror(errno), 1);
            return 1;
        }
    } else if (unlink(path) == -1) {
        mfm_show_message(path, 1);
        mfm_show_message(strerror(errno), 1);
        return 1;
    }
    return 0;
}

/**
 * Travers all selected items, except the current tab
 * @param st
 * @param cb
 * @param udata
 */
void mfm_traverse_all_selected(
    mfm_state* state,
    void (*cb)(mfm_tab*, mfm_tab_item*, void*),
    void* udata
) {
    for (int i = 0; i < state->len; i++) {
        if (state->cur == i) {
            continue;
        }
        mfm_tab* tab = state->tabs + i;
        for (int j = 0; j < tab->len; j++) {
            if (tab->items[j].props & MFM_SEL) {
                cb(tab, tab->items + j, udata);
            }
        }
    }
}

/**
 * Create the empty directory
 * @param name
 * @return
 */
int mfm_mkdir(char* name)
{
    return mkdir(name,
        S_IRUSR | S_IWUSR | S_IXUSR |
        S_IRGRP | S_IXGRP |
        S_IROTH | S_IXOTH
    );
}

/**
 * Get the text presentation of size in bytes
 * @param bytes
 * @param buf
 */
void mfm_get_size_text(unsigned long long bytes, char* buf)
{
    long long gb = 1 << 30, mb = 1 << 20, kb = 1 << 10;
    if (bytes > 1 << 30) { //GB and MB
        sprintf(buf, "%i Gb %i Mb", (int)(bytes / gb), (int)(bytes % gb / mb));
    } else if (bytes > 1 << 20) { //MB and KB
        sprintf(buf, "%i Mb %i Kb", (int)(bytes / mb), (int)(bytes % mb / kb));
    } else if (bytes > 1 << 10) { //KB and B
        sprintf(buf, "%i Kb %i b", (int)(bytes / kb), (int)(bytes % kb));
    } else { //B
        sprintf(buf, "%i b", (int)(bytes));
    }
}

/**
 * Drain all data at standard input
 */
void mfm_drain_input()
{
    char c;
    int fs = fcntl(0, F_GETFL, 0);
    fcntl(0, F_SETFL, fs | O_NONBLOCK);
    while (read(0, &c, 1) > 0);
    fcntl(0, F_SETFL, fs);
}

/**
 * Get command for file
 * @param file_name
 * @param file_commands_len
 * @param file_commands
 * @return
 */
char* mfm_get_command_for_file(
    char* file_name,
    int file_commands_len,
    char*** file_commands
) {
    int file_name_len = strlen(file_name);

    for (int i = 0; i < file_commands_len; i++) {
        char* extension = file_commands[i][0];
        int extension_len = strlen(extension);
        if (file_name_len < extension_len) {
            continue;
        }
        if (strcmp(file_name + file_name_len - extension_len, extension) == 0) {
            return file_commands[i][1];
        }
    }

    return NULL;
}

/**
 * Set cursor of tab on file
 */
void mfm_pos_tab_on_item(mfm_tab* tab, char* item)
{
    for (int i = 0; i < tab->len; i++) {
        if (strcmp(tab->items[i].text, item) == 0) {
            tab->act = i;
            break;
        }
    }
}

/**
 * Get directory item 
 */
char* mfm_get_directory_item(char* path, int index)
{
    DIR* dir = opendir(path);
    if (!dir) {
        return NULL;
    }
    struct dirent* entry;
    for (int i = 0; i <= index; i++) {
        entry = readdir(dir);
        if (entry) {
            if (mfm_dots_dir(entry->d_name)) {
                i--;
            }
        } else {
            closedir(dir);
            return NULL;
        }
    }
    char* result = mfm_append_paths(path, entry->d_name);
    closedir(dir);
    return result;
}
