#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <unistd.h>
#include <fcntl.h>

#include "mfm_general.h"
#include "mfm_input.h"
#include "mfm_tab.h"
#include "mfm_simple_menu.h"

#include "mfm_copy.h"

void mfm_copy_or_move(mfm_state* state, int op);

/**
 * Copy selected files from all tabs into current
 */
void mfm_copy(mfm_state* state)
{
    mfm_copy_or_move(state, 0);
}

/**
 * Move selected files from all tabs into current
 */
void mfm_move(mfm_state* state)
{
    mfm_copy_or_move(state, 1);
}

typedef struct
{
    int (*cb)(char*, struct stat*, void*);
    void* udata;
    void (*set_offset)(void*, int);
} mfm_selected_files_udata;

typedef struct
{
    int operation_type;
    unsigned long long really;
    unsigned long long ondisk;
    int conflict_all_action;
    unsigned long long current;
    char* dest;
    int offset;
} mfm_copy_files_udata;

void mfm_selected_files_cb(mfm_tab* tab, mfm_tab_item* item, void* udata);
void mfm_copy_files_set_offset(void* udata, int offset);
int mfm_files_size_cb(char* path, struct stat* stat, void* udata);
int mfm_copy_files_cb(char* path, struct stat* stat, void* udata);

/**
 * General copy operation
 * @param st
 */
void mfm_copy_or_move(mfm_state* state, int op)
{
    mfm_selected_files_udata cb;

    // Count size
    mfm_copy_files_udata copy_data;
    copy_data.operation_type = op;
    copy_data.dest = state->tabs[state->cur].dir;
    copy_data.ondisk = 0;
    copy_data.really = 0;
    cb.set_offset = mfm_copy_files_set_offset;
    cb.cb = mfm_files_size_cb;
    cb.udata = &copy_data;
    mfm_traverse_all_selected(state, mfm_selected_files_cb, &cb);

    // Check available free space
    struct statvfs sv;
    statvfs(state->tabs[state->cur].dir, &sv);
    unsigned long long free_bytes =
        (unsigned long long)sv.f_bsize *
        (unsigned long long)sv.f_bfree;
    if (free_bytes < copy_data.ondisk) {
        char buf[2][64];
        mfm_get_size_text(copy_data.ondisk, buf[0]);
        mfm_get_size_text(free_bytes, buf[1]);
        char format[] = "Not enough free space.\r\nNeed: %s\r\nAvailable: %s";
        char* message = malloc(strlen(format) + 1 - 2 - 2 + strlen(buf[0]) + strlen(buf[1]));
        sprintf(message, format, buf[0], buf[1]);
        mfm_show_message(message, 1);
        free(message);
        return;
    }

    // Copy files
    copy_data.conflict_all_action = 0; // No action for all conflicts
    copy_data.current = 0;
    cb.cb = mfm_copy_files_cb;
    cb.udata = &copy_data;
    mfm_traverse_all_selected(state, mfm_selected_files_cb, &cb);

    // Update tabs after operations
    for (int i = 0; i < state->len; i++) {
        if (chdir((state->tabs + i)->dir) != -1) {
            mfm_init_tab(
                state->tabs + i,
                state->file_commands,
                state->file_commands_len
            );
        }
    }
}

/**
 * Set offset to divide tab directory and rest part of file name
 */
void mfm_copy_files_set_offset(void* udata, int offset)
{
    mfm_copy_files_udata* copy_data = udata;
    copy_data->offset = offset;
}

/**
 * Count size of next selected item at any tab
 */
void mfm_selected_files_cb(mfm_tab* tab, mfm_tab_item* item, void* udata)
{
    mfm_selected_files_udata* cb = udata;
    char* full_path = mfm_append_paths(tab->dir, item->text);
    struct stat st;
    if (cb->set_offset) {
        int len = strlen(tab->dir);
        cb->set_offset(cb->udata, len == 1 ? 0 : len);
    }
    if (stat(full_path, &st) != -1) {
        cb->cb(full_path, &st, cb->udata);
    }
    free(full_path);
}

/**
 * Count size of next item
 */
int mfm_files_size_cb(char* path, struct stat* st, void* udata)
{
    mfm_copy_files_udata* copy_data = udata;
    if (copy_data->operation_type == 1) {
        char* dest = mfm_append_paths(
            copy_data->dest,
            path + copy_data->offset + 1
        );
        int rename_result = rename(path, dest);
        free(dest);
        if (rename_result != -1) {
            return 0;
        }
    }

    if (S_ISDIR(st->st_mode)) {
        mfm_traverse_directory(
            path,
            MFM_TRAVERS_WITH_STAT,
            mfm_files_size_cb,
            udata
        );
    } else {
        copy_data->ondisk += 512 * st->st_blocks;
        copy_data->really += st->st_size;
    }
    return 0;
}

int mfm_copy_single_file(
    char* dest,
    char* source,
    struct stat* st_source,
    mfm_copy_files_udata* copy_data
);
char* mfm_copy_resolve_conflict(char* dest, mfm_copy_files_udata* copy_data);

/**
 * Make copy operation on next selected item at any tab
 * @param path
 * @param st
 * @param udata
 */
int mfm_copy_files_cb(char* path, struct stat* st, void* udata)
{
    mfm_copy_files_udata* copy_data = udata;
    char* dest = mfm_append_paths(
        copy_data->dest,
        path + copy_data->offset + 1
    );
    struct stat st_source;
    if (stat(path, &st_source) == -1) {
        free(dest);
        return 0;
    }
    struct stat st_dest;
    int copy_result = 0;
    if (stat(dest, &st_dest) == -1) {
        if (S_ISDIR(st_source.st_mode)) {
            mkdir(dest, S_IRUSR | S_IWUSR | S_IXUSR);
            free(dest);
            dest = NULL;
            mfm_traverse_directory(path, MFM_TRAVERS_WITH_STAT, mfm_copy_files_cb, udata);
        } else if (S_ISREG(st_source.st_mode)) {
            copy_result = mfm_copy_single_file(dest, path, &st_source, copy_data);
        }
    } else if (S_ISDIR(st_dest.st_mode)) {
        if (S_ISDIR(st_source.st_mode)) {
            free(dest);
            dest = NULL;
            mfm_traverse_directory(path, MFM_TRAVERS_WITH_STAT, mfm_copy_files_cb, udata);
        } else if (S_ISREG(st_source.st_mode)) {
            dest = mfm_copy_resolve_conflict(dest, copy_data);
            if (dest) {
                mfm_delete_item(dest);
                copy_result = mfm_copy_single_file(dest, path, &st_source, copy_data);
            }
        }
    } else if ((dest = mfm_copy_resolve_conflict(dest, copy_data))) {
        mfm_delete_item(dest);
        if (S_ISREG(st_source.st_mode)) {
            copy_result = mfm_copy_single_file(dest, path, &st_source, copy_data);
        } else if (S_ISDIR(st_source.st_mode)) {
            mkdir(dest, st_source.st_mode);
            free(dest);
            dest = NULL;
            mfm_traverse_directory(path, MFM_TRAVERS_WITH_STAT, mfm_copy_files_cb, udata);
        }
    }
    free(dest);

    if (copy_data->operation_type == 1 && copy_result == 0) {
        int error = (S_ISREG(st_source.st_mode) ? unlink : rmdir)(path);
    }

    return 0;
}

void mfm_show_progress(mfm_copy_files_udata* copy_data);

/**
 * Write data from one file to other
 */
int mfm_copy_single_file(
    char* dest,
    char* source,
    struct stat* st_source,
    mfm_copy_files_udata* copy_data
) {
    int s = open(source, O_RDONLY);
    if (s == -1) {
        return -1;
    }

    int d = open(dest, O_WRONLY | O_CREAT, st_source->st_mode);
    if (d == -1) {
        close(s);
        return -1;
    }

    char buf[512];
    time_t current = time(NULL);
    ssize_t readed;
    do {
        readed = read(s, buf, 512);
        if (readed < 0) {
            break;
        }
        ssize_t to_write = readed;
        while (to_write) {
            ssize_t writed = write(d, buf + readed - to_write, to_write);
            if (writed < 0) {
                break;
            }
            to_write -= writed;
        }
        copy_data->current += readed;
        if (current != time(NULL)) {
            current = time(NULL);
            mfm_show_progress(copy_data);
        }
    } while (readed > 0);

    close(s);
    close(d);

    return 0;
}

/**
 * Resolve situation when 
 */
char* mfm_copy_resolve_conflict(char* dest, mfm_copy_files_udata* copy_data)
{
    switch (copy_data->conflict_all_action) {
        case 1: // Skip all
            free(dest);
            return NULL;
        case 2: // Replace all
            return dest;
    }

    char variants_texts[5][16] = {
        "Skip",
        "Skip all",
        "Replace",
        "Replace all",
        "Set new name"
    };

    char* variants[5] = {
        variants_texts[0],
        variants_texts[1],
        variants_texts[2],
        variants_texts[3],
        variants_texts[4]
    };

    printf("\e[37;49m\e[\e[2J\e[1;1HFile %s already exists", dest);
    int choice = mfm_simple_menu(variants, 5);
    switch (choice) {
        case 1:
            copy_data->conflict_all_action = 1;
        case -1:
        case 0:
            free(dest);
            return NULL;
        case 3:
            copy_data->conflict_all_action = 2;
        case 2:
            return dest;
        case 4: {
            char* last_part = strrchr(dest, '/');
            last_part = last_part ? last_part + 1 : dest;
            char* new_name = mfm_read_line(last_part);
            int pre_len = last_part - dest;
            dest = realloc(dest, pre_len + strlen(new_name) + 1);
            strcpy(dest + pre_len, new_name);
            free(new_name);
            return dest;
        }
    }

    return NULL;
}

/**
 * Show current progress of coping
 */
void mfm_show_progress(mfm_copy_files_udata* copy_data)
{
    char buf[2][64];
    mfm_get_size_text(copy_data->current, buf[0]);
    mfm_get_size_text(copy_data->really, buf[1]);
    char* format = "%s of %s";
    char* message = malloc(
        strlen(format) +
        1 -
        2 -
        2 +
        strlen(buf[0]) +
        strlen(buf[1])
    );
    sprintf(message, format, buf[0], buf[1]);
    mfm_show_message(message, 0);
    free(message);
}
