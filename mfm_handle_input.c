#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <search.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <stddef.h>

#include "mfm_general.h"
#include "mfm_handle_input.h"
#include "mfm_input.h"
#include "mfm_copy.h"
#include "mfm_goto.h"

int mfm_handle_key(
    mfm_key key,
    mfm_state* st
);
void mfm_mark(mfm_tab* tab);
void mfm_run_shell(mfm_state* st);
void mfm_action(mfm_tab* tab, char*** file_commands, int file_commands_len);
void mfm_rename(
    mfm_tab* tab,
    char*** file_commands,
    int file_commands_len
);
void mfm_delete(mfm_tab* tab);
void mfm_new_tab(mfm_state* st);
int mfm_jump(mfm_tab* tab);
int mfm_close_tab(mfm_state* st);
void mfm_exec_single(
    mfm_tab* tab,
    int h,
    int w,
    int delay
);
void mfm_edit_file(mfm_tab* tab);
void mfm_touch(mfm_tab* tab, char*** file_commands, int file_commands_len);

/**
 * Handle user input
 * @param st
 * @param h
 * @param w
 * @return
 */
int mfm_handle_input(mfm_state* st)
{
    //Current menu
    mfm_tab* tab = st->tabs + st->cur;

    //Current position in menu
    int act = tab->act;

    //Get user input
    char buf[8];
    mfm_key key;
    mfm_read_key(buf, &key);

    //Get the screen size
    int h, w;
    mfm_scr_size(&h, &w);

    //Is it necessary to reinit tab
    int ri = 0;
    //And to save active position
    int sa = 0;

    switch (buf[0]) {
        case '=':
        case '+':
            for (int i = 1; i < tab->len; i++) {
                tab->items[i].props |= MFM_SEL;
            }
            break;
        case '-':
        case '_':
            for (int i = 1; i < tab->len; i++) {
                tab->items[i].props &= ~MFM_SEL;
            }
            break;
        case ' ':
            mfm_mark(tab);
            break;
        case 13:
            mfm_action(tab, st->file_commands, st->file_commands_len);
            break;
        case 'q':
        case 'Q': //Exit from program
            return 1;
        case 's':
        case 'S': //Go to the shell
            mfm_run_shell(st);
            ri = 1;
            break;
        case 'r':
        case 'R': //Rename the file
            mfm_rename(tab, st->file_commands, st->file_commands_len);
            break;
        case '\t': //Create new tab
            mfm_new_tab(st);
            break;
        case 'u':
        case 'U':
            ri = 1;
            break;
        case 'd':
        case 'D': {
            char* new_dir = mfm_read_line(NULL);
            mfm_mkdir(new_dir);
            free(new_dir);
            ri = 1;
            break;
        }
        case 'h':
        case 'H':
            tab->hidden = !tab->hidden;
            ri = 1;
            break;
        case 'g':
        case 'G':
            mfm_goto(st->bookmarks, st->bookmarks_len);
            ri = 1;
            break;
        case 7:
            mfm_input_goto(h, w);
            ri = 1;
            break;
        case 'j':
        case 'J':
            tab->act = mfm_jump(tab);
            break;
        case 'c':
        case 'C':
            mfm_copy(st);
            break;
        case 'm':
        case 'M':
            mfm_move(st);
            break;
        case 23:
            return mfm_close_tab(st);
        case 'e':
        case 'E':
            mfm_edit_file(tab);
            break;
        case 'l':
        case 'L': {
            char* command = mfm_substitute(tab, "less %f");
            mfm_command(command);
            free(command);
            break;
        }
        case 't':
        case 'T':
            mfm_touch(tab, st->file_commands, st->file_commands_len);
            break;
        case 27:
            return mfm_handle_key(key, st);
    }
    if (ri) {
        mfm_init_tab(tab, st->file_commands, st->file_commands_len);
        if (sa) {
            tab->act = act;
        }
    }
    return 0;
}

/**
 * Handle the special key
 * @param key
 * @param st
 * @return
 */
int mfm_handle_key(
    mfm_key key,
    mfm_state* st
) {
    mfm_tab* tab = st->tabs + st->cur;
    int ri = 0;
    switch (key) {
        case MFM_KEY_UP:
            tab->act--;
            break;
        case MFM_KEY_DOWN:
            tab->act++;
            break;
        case MFM_KEY_LEFT:
            if (st->cur) {
                st->cur--;
            }
            break;
        case MFM_KEY_RIGHT:
            if (st->cur + 1 < st->len) {
                st->cur++;
            }
            break;
        case MFM_KEY_PGUP:
            tab->act -= 5;
            break;
        case MFM_KEY_PGDN:
            tab->act += 5;
            break;
        case MFM_KEY_HOME:
            tab->act = 0;
            break;
        case MFM_KEY_END:
            tab->act = tab->len - 1;
            break;
        case MFM_KEY_INSERT:
            mfm_mark(tab);
            break;
        case MFM_KEY_DELETE:
            mfm_delete(tab);
            ri = 1;
            break;
        default:
            break;
    }
    if (ri) {
        mfm_init_tab(tab, st->file_commands, st->file_commands_len);
    }
    return 0;
}

/**
 * Mark the menu element
 * @param tab
 */
void mfm_mark(mfm_tab* tab)
{
    //Current active element
    int act = tab->act;

    //Can't mark the ".." directory
    if (!act) {
        return;
    }

    //Mark or unmark the element
    if (tab->items[act].props & MFM_SEL) {
        tab->items[act].props &= ~MFM_SEL;
    } else {
        tab->items[act].props |= MFM_SEL;
    }

    //Go to next element
    tab->act++;
}

/**
 * Run shell
 * @param st
 */
void mfm_run_shell(mfm_state* st)
{
    //Current menu
    mfm_tab* tab = st->tabs + st->cur;

    //Current position in menu
    int act = tab->act;

    //Buffer for various purposes
    void* tmp;

    //Total lenfth of string
    int sum = 0;

    //Set files to env vars
    for (int i = 0; i < tab->len; i++) { //Count needed memory
        if (tab->items[i].props & MFM_SEL) {
            sum += strlen(tab->items[i].text) + 1;
        }
    }

    //Put data to the memory
    if (sum) {
        tmp = malloc(sum);
        sum = 0;
        for (int i = 0; i < tab->len; i++) {
            if (tab->items[i].props & MFM_SEL) {
                strcpy(tmp + sum, tab->items[i].text);
                sum += strlen(tab->items[i].text);
                ((char*)tmp)[sum] = '/';
                sum++;
            }
        }
        sum--;
        strcpy(tmp + sum, "\0");
        setenv("FF", tmp, 1);
        free(tmp);
    } else {
        setenv("FF", tab->items[act].text, 1);
    }

    //Run shell
    mfm_command(getenv("SHELL"));

    //Unset env. var
    setenv("FF", "", 1);
}

/**
 * Action on file:
 *  - enter to the directory
 *  - apply action to the file
 *  - execute executable
 * @param tab
 * @param file_commands
 * @param file_commands_len
 */
void mfm_action(mfm_tab* tab, char*** file_commands, int file_commands_len)
{
    int act = tab->act;

    //Go to the dir
    if (tab->items[act].props & MFM_DIR) {
        if (chdir(tab->items[act].text) != 0) {
            mfm_show_message(strerror(errno), 1);
        }
        mfm_init_tab(tab, file_commands, file_commands_len);
        return;
    }

    //Apply comman
    //d to the file of this type
    char* command = NULL;
    for (int i = 0; i < tab->len && !command; i++) {
        if (tab->items[i].props & MFM_SEL) {
            command = mfm_get_command_for_file(
                tab->items[i].text,
                file_commands_len,
                file_commands
            );
        }
    }

    if (!command) {
        command = mfm_get_command_for_file(tab->items[act].text, file_commands_len, file_commands);
    }

    if (command) {
        char* full_command = mfm_substitute(tab, command);
        mfm_command(full_command);
        free(full_command);
        return;
    }

    //If file is executable - run it
    if (tab->items[act].props & MFM_EXE) {
        void* tmp =
            malloc(strlen(tab->items[act].text) + 3);
        sprintf(tmp, "./%s", tab->items[act].text);
        mfm_command(tmp);
        free(tmp);
    }
}

/**
 * Rename the file
 * @param tab
 * @param h
 * @param w
 */
void mfm_rename(
    mfm_tab* tab,
    char*** file_commands,
    int file_commands_len
) {
    int act = tab->act;
    if (!act) {
        return;
    }
    char* new_name = mfm_read_line(tab->items[act].text);
    if (!new_name) {
        return;
    }
    if (rename(tab->items[act].text, new_name)) {
        free(new_name);
        return;
    }
    mfm_init_tab(tab, file_commands, file_commands_len);
    for (int i = 0; i < tab->len; i++) {
        if (!strcmp(tab->items[i].text, new_name)) {
            tab->act = i;
            break;
        }
    }
    free(new_name);
}

/**
 * Dialog with user with question like yes/no
 */
int mfm_ask_user_yn(char* message);

/**
 * Delete selected or under-cursor file or directory
 * @param tab
 * @param h
 * @param w
 */
void mfm_delete(mfm_tab* tab)
{
    if (!mfm_ask_user_yn("Are you sure? Y/n")) {
        return;
    }
    //If user confirmed - delete selected files
    mfm_show_message("Deleting...", 0);
    int ex = 0;
    for (int i = 0; i < tab->len; i++) {
        mfm_tab_item it = tab->items[i];
        if (it.props & MFM_SEL) {
            mfm_delete_item(it.text);
            ex = 1;
        }
    }

    //If no files selected - delete the current
    if (!ex) {
        int act = tab->act;
        mfm_delete_item(tab->items[act].text);
    }
}

/**
 * Dialog with user with question like yes/no
 */
int mfm_ask_user_yn(char* message)
{
    char buf[8];
    mfm_key key;
    int ex;

    //Ask user
    for (;;) {
        mfm_show_message("Are you sure? Y/N", 0);
        mfm_read_key(buf, &key);
        ex = 0;
        switch (*buf) {
        case 'y': case 'Y':
            ex = 2;
            break;
        case 'n': case 'N':
            ex = 1;
            break;
        case '\e':
            ex = !buf[1];
            break;
        }
        if (ex) {
            break;
        }
    }
    return ex == 2;
}

/**
 * Create the new tab
 * @param st
 */
void mfm_new_tab(mfm_state* st)
{
    if (st->len == 10) {
        return;
    }
    st->len++;
    st->tabs = realloc(
        st->tabs,
        sizeof(mfm_tab) * (st->len)
    );
    memset(st->tabs + st->len - 1, 0, sizeof(mfm_tab));
    mfm_init_tab(st->tabs + st->len - 1, st->file_commands, st->file_commands_len);
    st->cur = st->len - 1;
}

/**
 * Close current tab
 * @param st
 * @return
 */
int mfm_close_tab(mfm_state* st)
{
    int i = st->cur;
    mfm_destroy_tab(st->tabs + st->cur);
    st->len--;
    while (i < st->len) {
        st->tabs[i] = st->tabs[i + 1];
        i++;
    }
    if (!(st->len)) {
        return 1;
    }
    st->tabs = realloc(
        st->tabs,
        sizeof(mfm_tab) * st->len);
    if (st->cur >= st->len) {
        st->cur = st->len - 1;
    }
    return 0;
}

/**
 * Jump to the file or directory
 * 
 * @param tab
 * @param prop
 * @param h
 * @param w
 * @return finded position
 */
int mfm_jump(mfm_tab* tab)
{
    char* s = mfm_read_line(NULL);
    if (!s) {
        return tab->act;
    }
    int ls = strlen(s);

    int res = -1;
    for (int i = 0; i < tab->len; i++) {
        if (strncmp(s, tab->items[i].text, ls) == 0) {
            res = i;
            break;
        }
    }

    free(s);
    return res;
}

/**
 * Edit current files
 * @param tab
 */
void mfm_edit_file(mfm_tab* tab)
{
    char* editor = getenv("EDITOR");
    if (!editor) {
        return;
    }
    int ed_len = strlen(editor);
    if (!ed_len) {
        return;
    }
    char* format = malloc(ed_len + 3 + 1);
    sprintf(format, "%s %s", getenv("EDITOR"), "%f");
    char* command = mfm_substitute(tab, format);
    free(format);
    mfm_command(command);
    free(command);
}

/**
 * Create emty file if not exist
 * @param tab
 */
void mfm_touch(mfm_tab* tab, char*** file_commands, int file_commands_len)
{
    struct stat st;
    char* file_name = mfm_read_line(NULL);
    if (!file_name) {
        return;
    }
    char* full_file_name = mfm_append_paths(tab->dir, file_name);
    if (stat(full_file_name, &st) == -1) {
        FILE* f = fopen(full_file_name, "w");
        fclose(f);
    }
    free(full_file_name);
    mfm_init_tab(tab, file_commands, file_commands_len);
    mfm_pos_tab_on_item(tab, file_name);
    free(file_name);
}

